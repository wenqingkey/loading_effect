# loading_effect

![1](https://img-blog.csdnimg.cn/ceb13440c3fc4e539496aba47f5b9b52.gif)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
